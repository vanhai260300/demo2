1.Repository là gì? Phân biệt Remote repository và Local repository?
- Repository là Nơi chứa tất cả mã nguồn cho cho mooth dự án được quản lý bởi Git.
- Remote Repository là là repo để chia sẽ với nhiều người và được cài đặt trên server chuyên dụng.
- Local repo là là đưcọ cài đặt trên máy tính cá nhân, repo này sẽ dồng bộ hóa với remote repo bằng các lệnh Git.
2. Clone là gì? Để thực hiện clone cần yếu tố gì và sử dụng câu lệnh gì? 
Clone là sao chép một repo từ Remote về local, Từ thư mục này sang thư mục khác, copy một repo từ một URL 
- Yếu tố cần để thực hiện trên trên Git đã tồn tại một repo
- Câu lệnh thực hiện  git clone [url] 
3. Phân biệt các trạng thái của file sau: 
- Untracked – Trạng thái file không nằm trong danh sách theo dõi 
- Unmodified – các file được đánh dấu đã được commit 
- Committed – File đã được commit
- Unstaged hoặc Modified – Các file bị thay đổi – chưa commit
- Staged – Các file đã được thay đổi và được đánh dấu để commit 
- Và sử dụng lệnh gì để kiểm tra trạng thái file hiện tại? 
	git status hoặc git status –s (Ngắn gọn hơn)
4. Cách để thêm(add) một file vào trạng thái Staged là gì? Cách hủy bỏ chức năng add vừa rồi là gì? (undo add) 
- git add <file>  hoặc (git add . để add tất cả)
- git reset <file>
5. Lệnh git status -s để xem trạng thái của file dưới dạng ngắn gọn. Giải thích ý nghĩa của các ký hiệu trạng thái trước tên file: 
  M a.txt -  File đã có và đã add vào git
  M  b.txt – File có sửa đổi và chưa add
  MM c.txt – là nhưng file đã có và có sửa đổi chưa add vào git
  A  d.txt – File đã được add 
  AM  e.txt – File đã được add và có thay đổi
  ??  f.txt – File đã tạo mới và chưa có trên git
6. Giải thích ý nghĩa của lệnh git diff  và git diff --staged 
- git diff : Kiểm tra sự thay đổi của file đang làm việc
- git diff -staged : Kiểm tra sự thay đổi của thư mục và commit cuối.
7. Commit là gì? Câu lệnh đơn giản để thực hiện Commit là gì? Giải thích ý nghĩa của lệnh sau: 
- Commit: Thực hiện lưu vào toàn bộ CSDL Git toàn bộ nội dung chứa trong thư mục
- git commit -m"Description"
Giải thích ý nghĩa của lệnh sau: 
  	+ git rm -f file.txt: Dùng để xóa bỏ file file.txt trong thư mục hiện tại
	+ git rm -cached file.txt (biết rằng file.txt không phải là trạng thái untracked): Loại bỏ khỏi git
8. Mục đích Ignore là gì? Nêu cách Ignore file hoặc folder? 
- Mục đích của Ignore: liệt kê nhưng file không muốn cho vào git
- Cách Inore: Khi add 1 file mới vào git, git sẽ kiểm tra danh sách những file 
sẽ bỏ qua trong file .gitignore và không add chúng vào git và files không có trong 
git cache nữa thì git nó mới bỏ qua, chứ files mà nằm trong git cache thì .gitignore sẽ không có tác dụng. 
9. Giải thích ý nghĩa của lệnh sau: 
- git log : Xem lại thông tin lịch sửa đã commit
- git log --oneline : lịch sử của mỗi commit sẽ được rút gọn lại còn 1 dòng, it sẽ hiển thị tuần tự id và dòng commit message đầu tiên của commit.
- git show : Hiển thị nhiều loại đối tượng khác nhau (blobs, trees, tags and commits)
10. Giải thích ý nghĩa của lệnh sau: 
- git remote add origin <url> : tạo một điều khiển từ xa mới gọi là Origin đặt tại <url>
- git remote -v : xem remote bạn đã thêm
11. Branch là gì? Mục đích sử dụng Branch là gì? 
- Branch : là những phân nhánh ghi lại luồng thay đổi của lịch sử, các hoạt động trên mỗi branch sẽ không ảnh hưởng lên các branch khác 
- Mục đích : tiến hành nhiều thay đổi đồng thời trên một repository, giúp giải quyết được nhiều nhiệm vụ cùng lúc.
12. Nêu cách thực hiện: 
- Liệt kê các branch hiện tại : git branch
- Tạo mới một branch : git branch "branch_name"
- Chuyển tới một branch để làm việc : git checkout "branch_name"
- Xóa branch : git branch -d "branch_name"
- Đổi tên branch : git branch -m "newName"
13. Merge là gì? Nêu cách thực hiện 
- Merge : dùng merge để tích hợp 2 nhánh với nhau, câu lệnh merge sẽ lấy snapshot mới nhất của mỗi branch rồi combine với nhau để tạo ra một merge commit.
- Cách thực hiện : git checkout master => git merge "branch_name"
14. Rebase là gì? Nêu cách thực hiện? Sự khác biệt giữa Rebase và Merge ?
- Rebase : cũng gộp các commit từ nhánh này vào nhánh khác, bằng cách xây dựng lại các commit base kế thừa từ nhánh khác và 
viết lại lịch sử commit sau các commit cơ sở mới.
- Cách thực hiện : git checkout master => git rebase "branch_name"
- Khác nhau giữa merge và rebase : 
 	+ Rebase sẽ duyệt qua từng commit của nhánh đang làm việc và thực hiện thay đổi và trong qua trình làm việc.
	+ Merge là kết quả của khi trộn giữa 3 commit, đó là commit cha chung và 2 commit mới nhất của 2 nhánh, kết quả sẽ tạo ra một commit chứa kết quả của trộn 2 file
15. Conflict là gì? Conflict xảy ra trong những trường hợp nào và nêu cách giải quyết?
- Conflich là xung đột khi gộp lại
- Xảy ra khi hơn một người thay đổi một tệp nào đó.
- Chỉnh sửa nội dung dung đội sau đó thực hiện add , commit file đó.
16. Bạn có thể làm việc trên nhánh master không? Vì sao? Nếu bạn được giao làm một tính năng, bạn sẽ sử dụng quy trình làm việc của git như thế nào? 
- Có thể làm việc trên nhánh master nhưng Hạn chế Vì 1 project là có 1 team làm nếu làm trên master sẽ khó kiểm soát. 
- khi được giao một tính năng thì:
	+ Clone project về máy 
	+ Tạo Branch
	+ Add, commit lại sau mỗi lần hoàn thành nhiệm vụ
17. So sánh lệnh Fetch và Pull? 
- Fetch : tải về dữ liệu từ Remote Repo (các dữ liệu như các commit, các file, refs), dữ liệu tải về để bạn theo dõi sự thay đổi của remote, 
	tuy nhiên tải về bằng git fetch nó chưa tích hợp thay đổi ngay local repository của bạn, mục đích để theo dõi các commit người khác đã cập nhật lên server, 
	để có được thông thông tin khác nhau giữa remote và local
- Pull : lấy về thông tin từ remote và cập nhật vào các nhánh của local repo
18. Push là gì? 
- push : được sử dụng để đẩy các commit mới ở máy trạm (local repo) lên server (remote repo). Nguồn để đẩy lên là nhánh mà con trỏ HEAD đang trỏ tới (nhánh làm việc).
19. Merge request là gì? Sử dụng khi nào? 
- Merge request : là một yêu cầu từ một người nào đó để hợp nhất trong mã từ nhánh này sang nhánh khác
- Sử dụng khi yêu cầu hợp nhất các nội dung đã thay đổi
20. Revert là gì? Nêu cách hủy bỏ (revert) những gì đang thay đổi của một file, toàn bộ dự án? 
- Revert :  Trở lại các commit 
- git revert “idCommit”.
21. Giả sử có các commit theo thứ tự thời gian từ mới đến cũ: 
commit_Id_4 commit_Id_3 commit_Id_2 commit_Id_1 
 - Làm thế nào để revert tới commit_Id_2 ? : git revert commit_Id_2
22. Phân biệt 
- git reset --soft : Trường hợp này sẽ hủy commit cuối, con trỏ HEAD sẽ chuyển về commit cha. 
	Đồng thời những thay đổi của commit cuối được chuyển vào vùng staging nhằm để có cơ hội commit lại hoặc sửa đổi
- git reset --hard : thì kết quả giống với dùng tham số --soft, chỉ có một khác biết là nội dung thay đổi của commit cuối không đưa đưa vào staging mà bị hủy luôn
- git reset --mix : 
- git reset : Quay lại trạng thái file trước khi commit
23. Giải thích ý nghĩa của lệnh sau: 
- git clean -f 
 -> Xóa bỏ những file untracked.
- git clean -f –d
 -> Xóa bỏ file và forder untracked.
